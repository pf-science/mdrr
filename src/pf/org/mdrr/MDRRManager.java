package pf.org.mdrr;

import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: pf
 * Date: 11/25/12
 * Time: 12:41 AM
 * To change this template use File | Settings | File Templates.
 */
public class MDRRManager
{
    private ArrayList<PacketsQueue> queues;
    private ArrayList<Integer> schedule = new ArrayList<>();
    private int schedulerIndex = 0;

    public MDRRManager(ArrayList<PacketsQueue> _queues, int _mtu)
    {
        queues = _queues;
        for (PacketsQueue cpq: queues)
            cpq.setStep((int)Math.floor(_mtu * cpq.getWeight()));
        int prioritized = 0;
        for (int i = 0; i < queues.size(); i++)
            if (queues.get(i).isPrioritized())
            {
                prioritized = i;
                break;
            }
        for (int i = 0; i < queues.size(); i++)
            if (i != prioritized)
            {
                schedule.add(prioritized);
                schedule.add(i);
            }
    }

    public int getAlivePacketsCount()
    {
        int ret = 0;
        for (PacketsQueue cpq: queues)
            ret += cpq.getAlivePackets().size();
        return ret;
    }

    public boolean canProceed()
    {
        boolean ret = false;
        for (PacketsQueue cpq: queues)
            if (cpq.getPointer() < cpq.getLength())
                ret = true;
        return ret;
    }

    public ArrayList<Packet> getCurrentPackets()
    {
        ArrayList<Packet> ret = new ArrayList<>();

        PacketsQueue currentQueue = queues.get(schedule.get(schedulerIndex));
        currentQueue.setSeed(currentQueue.getSeed() + currentQueue.getStep());

        while (currentQueue.getSeed() > 0)
        {
            Packet currentPacket;
            do {
                currentPacket = currentQueue.getPacket(currentQueue.getPointer());
                currentQueue.setPointer(currentQueue.getPointer() + 1);
            } while (currentPacket != null && currentPacket.isDropped());

            if (currentPacket != null)
            {
                currentQueue.setSeed(currentQueue.getSeed() - currentPacket.getSize());
                ret.add(currentPacket);
            } else
                return ret;
        }

        return ret;
    }

    public void jumpToNext()
    {
        schedulerIndex++;
        if (schedulerIndex >= schedule.size())
            schedulerIndex = 0;
    }

    public int getCurrentQueue()
    {
        return schedule.get(schedulerIndex) + 1;
    }
}
