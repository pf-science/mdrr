package pf.org.mdrr;

import org.ini4j.Wini;

import java.io.File;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * User: pf
 * Date: 11/25/12
 * Time: 12:45 AM
 * To change this template use File | Settings | File Templates.
 */
public class ConfigurationReader
{
    private Wini configurationINI;

    public ConfigurationReader(String _fileName)
    {
        try
        {
            configurationINI = new Wini(new File(_fileName));
        } catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    public int getREDInitialSeed()
    {
        return configurationINI.get("red", "initial_seed", int.class);
    }

    public int getREDN()
    {
        return configurationINI.get("red", "n", int.class);
    }

    public int getREDK()
    {
        return configurationINI.get("red", "k", int.class);
    }

    public int getREDMmin()
    {
        return configurationINI.get("red", "mmin", int.class);
    }

    public int getREDMmax()
    {
        return configurationINI.get("red", "mmax", int.class);
    }

    public int getMDRRMTU()
    {
        return configurationINI.get("mdrr", "mtu", int.class);
    }
}
