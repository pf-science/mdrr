package pf.org.mdrr;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created with IntelliJ IDEA.
 * User: pf
 * Date: 11/21/12
 * Time: 9:59 PM
 * To change this template use File | Settings | File Templates.
 */
public class REDManager
{
    private ArrayList<PacketsQueue> queues;
    private int RavgPrev, n, k, Mmin, Mmax, stackSize = 1, stackPointer = 0;
    private Random RNG = new Random(System.currentTimeMillis());

    public REDManager(ArrayList<PacketsQueue> _queues, int _seed, int _n, int _k, int _Mmin, int _Mmax)
    {
        queues = _queues;
        RavgPrev = _seed;
        n = _n;
        k = _k;
        Mmin = _Mmin;
        Mmax = _Mmax;
    }

    public int getPacketsInStackSize()
    {
        int Rcurrent = 0;
        for (PacketsQueue queue: queues)
            if (!queue.isFullyDropped())
                for (int j = stackPointer; j < stackPointer + stackSize; j++)
                    if (queue.getPacket(j) != null)
                        if (!queue.getPacket(j).isDropped())
                            Rcurrent += queue.getPacket(j).getSize();
        return Rcurrent;
    }

    public int getPacketsInStackCount()
    {
        int packetsInStack = 0;
        for (PacketsQueue queue: queues)
            if (!queue.isFullyDropped())
                for (int j = stackPointer; j < stackPointer + stackSize; j++)
                    if (queue.getPacket(j) != null)
                        if (!queue.getPacket(j).isDropped())
                            packetsInStack++;
        return packetsInStack;
    }

    public int getRavgCurrent()
    {
        return (int)Math.floor(RavgPrev * (1.0 - 1.0 / Math.pow(2, n)) + getPacketsInStackSize() / Math.pow(2, n));
    }

    public void setRavgPrev(int _RavgPrev)
    {
        RavgPrev = _RavgPrev;
    }

    public double getP(int _RavgCurrent)
    {
        if (_RavgCurrent > Mmin && _RavgCurrent < Mmax)
            return ((double)_RavgCurrent - (double)Mmin) / ((double)Mmax - (double)Mmin) * 1.0 / (double)k;
        else if (_RavgCurrent <= Mmin)
            return 0;
        else if (_RavgCurrent >= Mmax)
            return 1;

        return 0;
    }

    public void jumpToNext()
    {
        stackPointer += stackSize;
        stackSize *= 2;
    }

    public boolean canProceed()
    {
        boolean ret = false;
        for (PacketsQueue cpq: queues)
            if (stackPointer < cpq.getLength())
                ret = true;
        return ret;
    }

    public int getPacketsToDrop(double _p)
    {
        return (int)Math.floor(_p * getPacketsInStackCount());
    }

    public int dropPackets(int _packetsToDrop)
    {
        int packetsDropped = 0;

        loop:
        while (packetsDropped < _packetsToDrop)
            for (PacketsQueue queue: queues)
                if (!queue.isFullyDropped())
                    for (int j = stackPointer; j < stackPointer + stackSize; j++)
                        if (queue.getPacket(j) != null)
                            if (!queue.getPacket(j).isDropped())
                                if (RNG.nextBoolean())
                                {
                                    queue.getPacket(j).setDropped();
                                    packetsDropped++;
                                    if (packetsDropped >= _packetsToDrop)
                                        break loop;
                                }
        return packetsDropped;
    }
}
