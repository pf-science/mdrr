package pf.org.mdrr;

import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: pf
 * Date: 11/21/12
 * Time: 10:25 PM
 * To change this template use File | Settings | File Templates.
 */
public class PacketsQueue
{
    private ArrayList<Packet> queue = new ArrayList<Packet>();
    private double weight;
    private boolean prioritized;
    private int pointer = 0, seed = 0, step = 0;

    public PacketsQueue(double _weight, boolean _prioritized)
    {
        weight = _weight;
        prioritized = _prioritized;
    }

    public void addPacket(Packet _packet)
    {
        queue.add(_packet);
    }

    public Packet getPacket(int _index)
    {
        if (_index < queue.size())
            return queue.get(_index);
        else
            return null;
    }

    public int getLength()
    {
        return queue.size();
    }

    public boolean isFullyDropped()
    {
        int droppedCount = 0;
        for (Packet cp: queue)
            if (cp.isDropped())
                droppedCount++;
        return droppedCount == queue.size();
    }

    public ArrayList<Packet> getAlivePackets()
    {
        ArrayList<Packet> ret = new ArrayList<Packet>();
        for (Packet cp: queue)
            if (!cp.isDropped())
                ret.add(cp);
        return ret;
    }

    public int getPointer()
    {
        return pointer;
    }

    public void setPointer(int _pointer)
    {
        pointer = _pointer;
    }

    public int getSeed()
    {
        return seed;
    }

    public int getStep()
    {
        return step;
    }

    public void setSeed(int _seed)
    {
        seed = _seed;
    }

    public void setStep(int _step)
    {
        step = _step;
    }

    public double getWeight()
    {
        return weight;
    }

    public boolean isPrioritized()
    {
        return prioritized;
    }
}
