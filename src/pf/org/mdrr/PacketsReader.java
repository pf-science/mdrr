package pf.org.mdrr;

import org.ini4j.Wini;

import java.io.*;
import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: pf
 * Date: 11/25/12
 * Time: 12:20 AM
 * To change this template use File | Settings | File Templates.
 */
public class PacketsReader
{
    private Wini metaINI;

    public PacketsReader(String _fileName)
    {
        try
        {
            metaINI = new Wini(new File(_fileName));
        } catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    public ArrayList<PacketsQueue> getQueues()
    {
        ArrayList<PacketsQueue> ret = new ArrayList<>();
        int queuesCount = metaINI.get("meta", "queues", int.class);
        for (int i = 1; i <= queuesCount; i++)
        {
            String currentQueueSection = "queue_" + String.valueOf(i);
            String currentQueueFileName = metaINI.get(currentQueueSection, "file", String.class);
            double currentQueueWeight = metaINI.get(currentQueueSection, "weight", double.class);
            boolean currentQueuePriority = metaINI.get(currentQueueSection, "prioritized", boolean.class);
            PacketsQueue currentPacketsQueue = new PacketsQueue(currentQueueWeight, currentQueuePriority);
            try
            {
                FileInputStream fis = new FileInputStream(currentQueueFileName);
                DataInputStream dis = new DataInputStream(fis);
                BufferedReader br = new BufferedReader(new InputStreamReader(dis));
                String size = "";
                while ((size = br.readLine()) != null)
                    currentPacketsQueue.addPacket(new Packet(Integer.valueOf(size)));
            } catch (IOException e)
            {
                e.printStackTrace();
            }
            ret.add(currentPacketsQueue);
        }
        return ret;
    }
}
