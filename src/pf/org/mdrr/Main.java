package pf.org.mdrr;

import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: pf
 * Date: 11/18/12
 * Time: 9:16 PM
 * To change this template use File | Settings | File Templates.
 */
public class Main
{
    public static void main(String[] argv)
    {
        PacketsReader packetsReader = new PacketsReader("queues.ini");
        ArrayList<PacketsQueue> queues = packetsReader.getQueues();

        ConfigurationReader configurationReader = new ConfigurationReader("cfg.ini");

        REDManager redManager = new REDManager(queues, configurationReader.getREDInitialSeed(), configurationReader.getREDN(), configurationReader.getREDK(), configurationReader.getREDMmin(), configurationReader.getREDMmax());
        int redStep = 1;
        while (redManager.canProceed())
        {
            System.out.printf("===RED, крок %d===\n", redStep++);
            int RavgCurrent = redManager.getRavgCurrent();
            System.out.printf("Rсер.пот.=%d\n", RavgCurrent);
            double p = redManager.getP(RavgCurrent);
            System.out.printf("p=%f\n", p);
            int packetsToDrop = redManager.getPacketsToDrop(p);
            System.out.printf("Пакетів до відкидання: %d\n", packetsToDrop);
            System.out.printf("Відкинуто пакетів: %d\n", redManager.dropPackets(packetsToDrop));

            redManager.setRavgPrev(RavgCurrent);
            redManager.jumpToNext();
        }

        MDRRManager MDRRmanager = new MDRRManager(queues, configurationReader.getMDRRMTU());
        System.out.printf("Залишилося пакетів: %d\n", MDRRmanager.getAlivePacketsCount());
        System.out.println("===MDRR===");
        int processedPackets = 0;
        while (MDRRmanager.canProceed())
        {
            ArrayList<Packet> currentPackets = MDRRmanager.getCurrentPackets();
            if (!currentPackets.isEmpty())
            {
                for (Packet cmp: currentPackets)
                    System.out.printf("Черга %d, розмір пакета: %d\n", MDRRmanager.getCurrentQueue(), cmp.getSize());
                processedPackets += currentPackets.size();
            }
            MDRRmanager.jumpToNext();
        }
        System.out.printf("Оброблено пакетів: %d\n", processedPackets);
    }
}
