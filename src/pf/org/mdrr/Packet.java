package pf.org.mdrr;

/**
 * Created with IntelliJ IDEA.
 * User: pf
 * Date: 11/18/12
 * Time: 9:19 PM
 * To change this template use File | Settings | File Templates.
 */
public class Packet
{
    private int size;
    private boolean dropped = false;

    public Packet(int _size)
    {
        size = _size;
    }

    public int getSize()
    {
        return size;
    }

    public void setDropped()
    {
        dropped = true;
    }

    public boolean isDropped()
    {
        return dropped;
    }
}
